﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Outlook = Microsoft.Office.Interop.Outlook;
using Office = Microsoft.Office.Core;

namespace OutlookAddIn1
{

    public partial class ThisAddIn
    {
        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
			//portion where the start up location worked
			//maintains a reference to the collection of Inspector windows in the current Outlook instance
			Outlook.Inspectors inspectors;
            Outlook.Application application;

			inspectors = this.Application.Inspectors;
            application = this.Application;

            inspectors.NewInspector +=
			new Microsoft.Office.Interop.Outlook.InspectorsEvents_NewInspectorEventHandler(Inspectors_NewInspector);//for the new MSG

            //for the added feature at the Before Send
            application.ItemSend +=
            new Outlook.ApplicationEvents_11_ItemSendEventHandler(ItemSend_BeforeSend);
        }

		private void Inspectors_NewInspector(Outlook.Inspector Inspector)
		{
			Outlook.MailItem mailItem = Inspector.CurrentItem as Outlook.MailItem;
			if (mailItem != null)
			{
				if (mailItem.EntryID == null)
				{
					mailItem.Subject = "This text was added by using code";
					mailItem.Body = "This text was added by using code";
				}

                //a potion for theselected words


			}
		}

        //function for getting the send Message Before intefre

        private void ItemSend_BeforeSend(Object item,ref bool cancel)
        {
            Outlook.MailItem mailItem = (Outlook.MailItem)item;
            if (mailItem != null)
            {
                mailItem.Subject += "Ben Injected";
                mailItem.Body += "Injeced String with the recving";
                //api call and set response mailItem.Body = apicall
            }
            cancel = false;
        }
		private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
            // Note: Outlook no longer raises this event. If you have code that 
            //    must run when Outlook shuts down, see https://go.microsoft.com/fwlink/?LinkId=506785
        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion
    }
}
